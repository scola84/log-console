'use strict';

const DI = require('@scola/di');
const Log = require('@scola/log');
const Logger = require('./lib/logger');

class Module extends DI.Module {
  configure() {
    this.addModule(Log.Module);

    this.inject(Logger).with(this.object({
      close: this.instance(require('./lib/loggers/close')),
      debug: this.instance(require('./lib/loggers/debug')),
      error: this.instance(require('./lib/loggers/error')),
      timer: this.instance(require('./lib/loggers/timer'))
    }));
  }
}

module.exports = {
  Log,
  Logger,
  Module
};
