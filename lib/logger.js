'use strict';

class ConsoleLogger {
  constructor(loggers) {
    this.loggers = loggers;
    this.filter = [];
  }

  getFilter() {
    return this.filter;
  }

  setFilter(filter) {
    this.filter = filter;
    return this;
  }

  log(event, ...parameters) {
    this.loggers[event]
      .get()
      .setFilter(this.filter)
      .log(...parameters);

    return this;
  }
}

module.exports = ConsoleLogger;
