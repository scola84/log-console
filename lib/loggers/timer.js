'use strict';

const AbstractLogger = require('./abstract');

class TimerLogger extends AbstractLogger {
  log(time) {
    this.print(
      '[timer]',
      '[' + time.getName() + ']',
      '[' + time.getId() + ']',
      time.getValue() + 'ms'
    );
  }
}

module.exports = TimerLogger;
