'use strict';

const AbstractLogger = require('./abstract');

class CloseLogger extends AbstractLogger {
  log(source, connection, code) {
    this.print(
      '[close]',
      '[' + source + ']',
      '[' + connection.getId() + ']',
      code || ''
    );
  }
}

module.exports = CloseLogger;
