'use strict';

class AbstractLogger {
  constructor() {
    this.filter = null;
  }

  getFilter() {
    return this.filter;
  }

  setFilter(filter) {
    this.filter = filter;
    return this;
  }

  print(...parameters) {
    console.log(
      '[' + new Date().toISOString() + ']',
      ...parameters
    );
  }

  log() {
    throw new Error('not_implemented');
  }
}

module.exports = AbstractLogger;
