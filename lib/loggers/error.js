'use strict';

const AbstractLogger = require('./abstract');

class ErrorLogger extends AbstractLogger {
  log(source, error) {
    if (error.origin) {
      this.log(source, error.origin);
    }

    this.print(
      '[error]',
      '[' + source + ']',
      '[' + error.id + ']',
      error.stack,
      error.detail
    );
  }
}

module.exports = ErrorLogger;
