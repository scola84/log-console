'use strict';

const AbstractLogger = require('./abstract');

class CloseLogger extends AbstractLogger {
  log(source, instance, method, ...info) {
    const item = instance.constructor.name + '.' + method;

    if (this.filter.indexOf(item) === -1) {
      info = [];
    }

    this.print(
      '[debug]',
      '[' + source + ']',
      '[' + item + ']',
      ...info
    );
  }
}

module.exports = CloseLogger;
